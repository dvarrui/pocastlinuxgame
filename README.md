
# PodcastLinuxGame

## TO-DO list

* Una interfaz con las vidas y monedas
* Un tiempo por nivel
* Una imagen de inicio con botones...
* Etc.

## Contribuciones

Puedes enviar tus contribuciones mediante `issues` o `fork`.

**Issues**:
* Se pueden usar para tratar asuntos sobre el proyecto. Como por ejemplo, ideas, peticiones de cambio, nuevas características, incidencias, etc.
* El URl para crear una nueva issue es https://gitlab.com/podcastlinux/pocastlinuxgame/-/issues/

**Fork**
* Podemos hacer un `fork` del repositorio original (https://gitlab.com/podcastlinux/pocastlinuxgame/-/forks/new).
* A partir de ahora tenemos, trabajaremos en nuestro repositorio clonado.
* Cuando terminemos los cambios creamos un `merge request` desde nuestro proyecto clonado.
* Ahora hay que esperar que se acepte la petición para ver los cambios en el repositorio original.
