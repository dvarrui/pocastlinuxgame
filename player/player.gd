extends KinematicBody2D


const ACCELERATION = 100
const MAX_SPEED = 60
const JUMP_H = -500
const UP = Vector2(0, -0.5)
const GRAVITY = 50  

onready var sprite = $Sprite
onready var	animationPlayer = $AnimationPlayer



var motion = Vector2()

func _physics_process(_delta):
	motion.y += GRAVITY
	var FRICTION = false
	if Input.is_action_pressed("ui_right"):
		sprite.flip_h = false
		animationPlayer.play("walk")
		motion.x = min(motion.x + ACCELERATION, MAX_SPEED)
	elif Input.is_action_pressed("ui_left"):
		sprite.flip_h = true
		animationPlayer.play("walk")
		motion.x = max(motion.x - ACCELERATION, -MAX_SPEED)
	else:
		animationPlayer.play("idle")
		FRICTION = true
		
	if is_on_floor():
		
		if Input.is_action_pressed("ui_accept"):
			motion.y = JUMP_H
		if FRICTION == true:
			motion.x = lerp(motion.x, 0, 0.5)
	else:
			if FRICTION == true:
				motion.x = lerp(motion.x, 0, 0.01)	
				
	motion = move_and_slide(motion, UP)


func _on_Area2D_body_entered(body):
	if body.is_in_group("enemy"):
		get_tree().reload_current_scene()


func _on_VisibilityNotifier2D_screen_exited():
	get_tree().quit()
