extends KinematicBody2D

const MAX_SPEED : float = 30.0
const GRAVITY : float = 25.0  

var motion := Vector2()
func _ready():
	$AnimatedSprite.scale.x = 1
	motion.x = MAX_SPEED
	pass
	
	
func _next_to_left_wall() -> bool:
	return $leftRay.is_colliding()	

func _next_to_rigth_wall() -> bool:
	return $rigthRay.is_colliding()	
	
func _floor_detection() -> bool:
	return $AnimatedSprite/floorDetection.is_colliding()
	
func _flip():
	if _next_to_rigth_wall() or _next_to_left_wall() or !_floor_detection():
	 motion.x *= -1
	 $AnimatedSprite.scale.x *= -1

func _process(_delta):
	motion.y += GRAVITY
	_flip()
	
	motion = move_and_slide(motion)
	pass


func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		get_tree().change_scene("res://level/level.tscn")
